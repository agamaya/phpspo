<?php
namespace Office365\PHP\Client\SharePoint;
use Office365\PHP\Client\Runtime\ClientValueObject;

/**
 * Represents properties that can be set when creating a field.
 */
class FieldCreationInformation extends ClientValueObject
{


    public function __construct($title)
    {

        $this->Title = $title;
//        $this->DisplayName = $title;
//        $this->Description = $title;
//        $this->TypeDisplayName = $title;
        $this->FieldTypeKind = '2';
//        $this->CanBeDeleted = '';
//        $this->DefaultValue = '';

//        $this->StaticName = $title;

//        $this->EntityPropertyName = $title;

//        $this->InternalName = $title;

//        $this->LookupList = '{4635daeb-7206-4513-ad17-ea06e09187ad}';
//        $this->LookupListId = '{4635daeb-7206-4513-ad17-ea06e09187ad}';
//        $this->LookupField = $title;
//        $this->LookupFieldName = $title;


        parent::__construct("Field");
//        parent::__construct("FieldLookup");
    }

    
    /**
     * @var bool
     */
    public $CanBeDeleted;


    /**
     * @var string
     */
    public $DefaultValue;

    /**
     * @var string
     */
    public $Title;

    /**
     * @var string
     */
    public $Description;

    /**
     * @var int
     */
    public $FieldTypeKind;

    /**
     * @var string
     */
    public $TypeDisplayName;

    public $StaticName;

    public $EntityPropertyName;

    public $InternalName;

    public $LookupField;

    public $LookupList;
    public $LookupWebId;
    public $PrimaryFieldId;

    public $SchemaXml;
    public $LookupListId;
    public $LookupFieldName;
}